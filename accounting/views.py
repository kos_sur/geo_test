from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import CreateView, ListView
from django_tables2 import RequestConfig

from accounting.forms import OrderForm
from accounting.models import Order
from accounting.tables import OrderTable
from accounting.tasks import calculate_location


class OrderCreateView(CreateView):
    model = Order
    form_class = OrderForm

    def form_valid(self, form):
        order = form.save()

        # Starting celery task
        calculate_location.apply_async(args=(order.pk,))

        # Updating session with the success message
        messages.add_message(self.request, messages.INFO,
                             "Order #{} ({}) have been successfully created!"
                             .format(order.pk, order.name))

        return redirect("accounting:order-track")


class OrderTrackView(ListView):
    queryset = Order.objects.all()
    context_object_name = 'orders'
    template_name = "accounting/order_track.html"

    def get_context_data(self, **kwargs):
        context = super(OrderTrackView, self).get_context_data(**kwargs)
        orders = context.get(self.context_object_name)
        orders = orders.order_by("distance")

        # Initializing Table and applying sorts
        table = OrderTable(orders)
        RequestConfig(self.request).configure(table)

        context.update(table=table)
        return context


order_create = OrderCreateView.as_view()
order_track = OrderTrackView.as_view()
