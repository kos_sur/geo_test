from django_tables2 import Table

from accounting.models import Order


class OrderTable(Table):

    def render_location(self, record, **kwargs):
        long, lat = record.location.get_coords()
        return "{} {}".format(long, lat)

    class Meta:
        model = Order
        attrs = {"class": "table table-hover"}
