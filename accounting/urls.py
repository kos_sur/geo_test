from django.conf.urls import url

from . import views


urlpatterns = [
    url(r"^$", views.order_create, name="order-create"),
    url(r"^orders/track/$", views.order_track, name="order-track"),
]
