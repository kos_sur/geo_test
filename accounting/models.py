from django.contrib.gis.db.models import PointField, GeoManager
from django.db import models


class Order(models.Model):
    # STATUS CHOICES
    PENDING = 0
    COMPLETED = 1

    STATUS_CHOICES = (
        (PENDING, "Pending"),
        (COMPLETED, "Completed"),
    )

    name = models.CharField(max_length=255, verbose_name="Name")
    status = models.IntegerField(choices=STATUS_CHOICES, default=PENDING,
                                 verbose_name="Status")
    location = PointField(geography=True, verbose_name="Location (long, lat)")
    distance = models.FloatField(verbose_name="Distance (km)",
                                 help_text="Calculated distance "
                                           "between order's location"
                                           " and New York",
                                 blank=True, null=True)

    updated_at = models.DateTimeField(auto_now=True, verbose_name="Updated At")
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name="Created At")

    objects = GeoManager()

    def __str__(self):
        return self.name
