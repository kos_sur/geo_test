from django import forms
from mapwidgets.widgets import GooglePointFieldWidget

from accounting.models import Order


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ("name", "location",)
        widgets = {
            "location": GooglePointFieldWidget,
        }
