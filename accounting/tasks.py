import logging
import random
import time

from celery import shared_task
from django.conf import settings
from django.contrib.gis.geos import Point

from accounting.models import Order

logger = logging.getLogger(__name__)


@shared_task
def calculate_location(order_pk):
    """
    Sleeps for a random amount of time in range 10-30 secs
    And calculates distance for the order using postGIS
    :param order_pk: Order ID to perform calculations for
    """
    # Simulating heavy calculations
    sec_to_sleep = random.randint(10, 30)
    time.sleep(sec_to_sleep)

    logger.info("Distance calculation for order #{} have been started"
                .format(order_pk))

    # New York coordinates
    base_point = Point(settings.BASE_COORDINATES, srid=4326)

    # Annotating GeoQueryset with distance
    order = (Order.objects
             .filter(pk=order_pk)
             .distance(base_point)
             .first())
    distance = order.distance.km

    # Finally, updating order with the new status and distance
    order.status = Order.COMPLETED
    order.distance = distance
    order.save(update_fields=["status", "distance", ])

    logger.info("Distance calculation for order #{} have been finished "
                "(distance: {} km)".format(order_pk, distance))
