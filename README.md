### Description ###

Simple Django app for submitting and tracking Orders that have a location.
On the background celery calculates distance between New York (41.145495, −73.994901)
and Order's location. Also, you can track the order's status and calculated distance. 


### Technologies Used ###
* Python / Django
* PostgreSQL (with PostGIS)
* Twitter Bootstrap 3
* Google Maps API
* Celery (with Redis)